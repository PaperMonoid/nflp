from django.shortcuts import render
from reportes.models import Jugador, Equipo, Estadio


def jugador_detalles(request, id):
    jugador = Jugador.objects.get(id=id)
    jugador.actualizar_equipos()

    contexto = {"jugador": jugador}
    return render(request, "reportes/jugador/detalles.html", contexto)


def jugador_lista(request):
    if request.POST.get("q"):
        tipo = request.POST.get("tipo")
        busqueda = request.POST.get("q")
        if tipo == "equipo":
            equipos = Equipo.objects.filter(nombre=busqueda)
            equipo = equipos[0] if (len(equipos) > 0) else None
            id = equipo and equipo.id or None
            jugadores = Jugador.objects.filter(equipo_actual=id)
        elif tipo == "posicion":
            jugadores = Jugador.objects.filter(posicion=busqueda)
        elif tipo == "cambio":
            equipos = Equipo.objects.filter(nombre=busqueda)
            equipo = equipos[0] if (len(equipos) > 0) else None
            id = equipo and equipo.id or None
            jugadores = Jugador.objects.filter(equipo_actual=id).exclude(
                equipo_anterior=None
            )
        elif tipo == "despedidos":
            equipos = Equipo.objects.filter(nombre=busqueda)
            equipo = equipos[0] if (len(equipos) > 0) else None
            id = equipo and equipo.id or None
            jugadores = Jugador.objects.filter(equipo_anterior=id)
        else:
            jugadores = Jugador.objects.all()
    else:
        jugadores = Jugador.objects.all()

    for jugador in jugadores:
        jugador.actualizar_equipos()

    contexto = {"jugadores": jugadores}
    return render(request, "reportes/jugador/lista.html", contexto)


def estadio_detalles(request, id):
    estadio = Estadio.objects.get(id=id)

    contexto = {"estadio": estadio}
    return render(request, "reportes/estadio/detalles.html", contexto)


def estadio_lista(request):
    estadios = Estadio.objects.all()

    contexto = {"estadios": estadios}
    return render(request, "reportes/estadio/lista.html", contexto)


def equipo_detalles(request, id):
    equipo = Equipo.objects.get(id=id)

    contexto = {"equipo": equipo}
    return render(request, "reportes/equipo/detalles.html", contexto)


def equipo_lista(request):
    equipos = Equipo.objects.all()

    contexto = {"equipos": equipos}
    return render(request, "reportes/equipo/lista.html", contexto)
