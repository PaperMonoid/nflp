from django.contrib import admin
from reportes.models import Equipo, Estadio, Jugador

admin.site.register(Equipo)
admin.site.register(Estadio)
admin.site.register(Jugador)
