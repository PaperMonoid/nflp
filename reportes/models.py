from django.db import models


class Jugador(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    posicion = models.CharField(max_length=50)
    equipo_actual = models.IntegerField(null=True, blank=True)
    equipo_anterior = models.IntegerField(null=True, blank=True)

    def actualizar_equipos(self):
        if self.equipo_actual:
            equipo = Equipo.objects.get(id=self.equipo_actual)
            self.equipo_actual = equipo.nombre
        else:
            self.equipo_actual = "Ninguno"

        if self.equipo_anterior:
            equipo = Equipo.objects.get(id=self.equipo_anterior)
            self.equipo_anterior = equipo.nombre
        else:
            self.equipo_anterior = "Ninguno"


class Equipo(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    estadio = models.IntegerField()
    super_bowls = models.IntegerField()

    @property
    def nombre_estadio(self):
        estadio = Estadio.objects.get(id=self.estadio)
        if estadio:
            return estadio.nombre
        else:
            return ""

    @property
    def miembros(self):
        return Jugador.objects.filter(equipo_actual=self.id)


class Estadio(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    ciudad = models.CharField(max_length=50)
    estado = models.CharField(max_length=50)
    capacidad = models.IntegerField()
