from django.urls import path
from reportes import views

urlpatterns = [
    path("jugador", views.jugador_lista, name="jugador_lista"),
    path("jugador/<int:id>", views.jugador_detalles, name="jugador_detalles"),
    path("estadio", views.estadio_lista, name="estadio_lista"),
    path("estadio/<int:id>", views.estadio_detalles, name="estadio_detalles"),
    path("equipo", views.equipo_lista, name="equipo_lista"),
    path("equipo/<int:id>", views.equipo_detalles, name="equipo_detalles"),
]
